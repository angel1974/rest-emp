package com.techuniversity.emp.model;

import java.util.ArrayList;

public class Empleado {
    public Empleado(Integer id, String nombre, String apellidos, String email, ArrayList<Formacion> formaciones) {
        Id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.email = email;
        this.formaciones=formaciones;
    }

    @Override
    public String toString() {
        return "Empleado{" +
                "Id=" + Id +
                ", nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", email='" + email + '\'' +
                '}';
    }



    private Integer Id;
    private String nombre;
    private String apellidos;
    private String email;
    private ArrayList<Formacion> formaciones;

    public ArrayList<Formacion> getFormaciones() {
        return formaciones;
    }

    public void setFormaciones(ArrayList<Formacion> formaciones) {
        this.formaciones = formaciones;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

